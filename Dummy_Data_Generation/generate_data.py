#Script to Generate the random data for testing purpose
#Author:  Amogh Babu K A, Hari Prasad
#Company: SIXDEE TECHNOLOGIES

#required Libraries
from random import randint,seed
import datetime,csv,random
from _datetime import timedelta

#to get the random values
def random_function():
    num = randint(0,10)
    return(num)

#function to get random channels
def random_channels():
    channel =["MOBAPP","WEB"]
    chan_value = random.choice(channel)
    return(chan_value)

#function to get random users
def random_user():
    names =["Amogh Babu","Hari Prasad","Salal","ajit"]
    user = random.choice(names)
    return(user)

# Main function or logic to get the work done 
if __name__ == '__main__':
    txn_id=100288200000000
    MSISDN=77000000
    days=1 
    start_date=datetime.datetime(2019,6,1)
    txn_list = []
    print("---------------Sit Back and Relax Data is getting generated----------------")
    while days <= 120:
        for i in list(range(0,500)) :
            txn_id+=1
            MSISDN+=1
            txn_list = [txn_id,start_date,random_function(),random_channels(),"26","CASH_IN","10","1182",MSISDN,"2","2","super agent","4","20.28","10.28","24","super test","NULL","1194",MSISDN,"1","0","Masster Agent","4","195.95","201.75",random_user(),"NULL","1000"]
            with open('dummy_data.csv','a',newline='') as file:
                writer = csv.writer(file,lineterminator='\n')
                writer.writerow(txn_list)
            i+=1
        start_date= start_date + timedelta(days=1)
        days=days+1
print("-----------Finally dummy values got generated End of CDR Script----------------")